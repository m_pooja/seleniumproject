package Test_cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Common_Function.Handle_Browser;

public class CalendarWithImplicity {

	public static void main(String[] args) {
		WebDriver Driver = Handle_Browser.InstantiateBrowser("Chrome");
		 Handle_Browser.LaunchURL(Driver, "https://www.redBus.com/");
		
		 WebElement DepartDate = Driver.findElement(By.xpath("//input[@placeholder=\"DD/MY\"]"));
		 
		 DepartDate.click();
		 
		 WebElement CurrentDate = Driver.findElement(By.xpath("//*[@id='rdc-root']/div/div[1]/div[2]/div[2]/div/div/div[1]/span/span[5]/div[3]/span"));
		 CurrentDate.click();
		 
		 WebElement cookie = Driver.findElement(By.xpath("//*[@id='cookie-root']/div/div/div/button"));
			cookie.click();

	}

}
