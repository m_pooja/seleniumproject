package Common_Function;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Handle_Browser {
	public static WebDriver InstantiateBrowser(String Browser) {
		WebDriver driver;
		switch (Browser) {
		case "Chrome":
			driver = new ChromeDriver();
			break;
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Edge":
			driver = new EdgeDriver();
			break;
		default:
			driver = new ChromeDriver();

		}
		return driver;
	}

	public static void LaunchURL(WebDriver driver, String URL) {
		driver.get(URL);
		driver.manage().window().maximize();

	}

	public static void Validate_Click(WebDriver driver, WebElement customerService, int i) {
		driver.manage().window().maximize();

		// driver.manage().window().maximize();)

	}

}
